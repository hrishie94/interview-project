const express = require('express')
const port = 8000;
const app = express();
app.use(express.json())
app.use(express.urlencoded({
  extended: true
}));
const adminroute = require("./route/adminRoute")
app.use("/api",adminroute)
const departmentRoute = require("./route/departmentRoute")
app.use("/api",departmentRoute)
app.listen(port, () => {
  console.log('socket server is listening at port ' + port)
})